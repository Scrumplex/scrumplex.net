export type IconsId =
  | "00-baseline"
  | "archlinux"
  | "bitcoin"
  | "codeberg"
  | "github"
  | "gitlab"
  | "ko-fi"
  | "liberapay"
  | "mail"
  | "matrix"
  | "new_tab"
  | "paypal"
  | "phone"
  | "teamspeak"
  | "telegram"
  | "wire";

export type IconsKey =
  | "i00Baseline"
  | "Archlinux"
  | "Bitcoin"
  | "Codeberg"
  | "Github"
  | "Gitlab"
  | "KoFi"
  | "Liberapay"
  | "Mail"
  | "Matrix"
  | "NewTab"
  | "Paypal"
  | "Phone"
  | "Teamspeak"
  | "Telegram"
  | "Wire";

export enum Icons {
  i00Baseline = "00-baseline",
  Archlinux = "archlinux",
  Bitcoin = "bitcoin",
  Codeberg = "codeberg",
  Github = "github",
  Gitlab = "gitlab",
  KoFi = "ko-fi",
  Liberapay = "liberapay",
  Mail = "mail",
  Matrix = "matrix",
  NewTab = "new_tab",
  Paypal = "paypal",
  Phone = "phone",
  Teamspeak = "teamspeak",
  Telegram = "telegram",
  Wire = "wire",
}

export const ICONS_CODEPOINTS: { [key in Icons]: string } = {
  [Icons.i00Baseline]: "61697",
  [Icons.Archlinux]: "61698",
  [Icons.Bitcoin]: "61699",
  [Icons.Codeberg]: "61700",
  [Icons.Github]: "61701",
  [Icons.Gitlab]: "61702",
  [Icons.KoFi]: "61703",
  [Icons.Liberapay]: "61704",
  [Icons.Mail]: "61705",
  [Icons.Matrix]: "61706",
  [Icons.NewTab]: "61707",
  [Icons.Paypal]: "61708",
  [Icons.Phone]: "61709",
  [Icons.Teamspeak]: "61710",
  [Icons.Telegram]: "61711",
  [Icons.Wire]: "61712",
};
